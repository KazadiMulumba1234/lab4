#!/bin/sh

 # Validate LAB4_VAR
if [ "$LAB4_VAR" != "lab 4" ]; then
echo "Environment variable LAB4_VAR is not set or has an incorrect value."
exit 1
fi

if [ -e uuidgen.sh ];  then
    :
else
    echo "File uuidgen.sh not found. Either run.sh or uuidgen.sh is in the wrong location.";
    exit 1
fi

# Validate VER_VAR
if [ "$VER_VAR" != $(./uuidgen.sh) ]; then
    echo "Environment variable VER_VAR is not set or has an incorrect value."
    exit 1
fi

if [ -x lab4.sh ];  then
    # Delete the webpages directory if it exists before running script
    if [ -d webpages ]; then
        rm -r webpages
    fi 

    chmod a-x urlparser.py

    # Run the script
    ./lab4.sh webpages urlparser.py html < urls.txt

    # Verify that the webpages directory is created properly
    if [ -d webpages ]; then

        # Verify that each file is created and is non-empty
        files=$(ls webpages)
        for file in $files; do
            if [ -f webpages/$file ]\
                && [ -s webpages/$file ]\
                && [ $(./urlparser.py "$file") = "html" ]; then
                :
            else
                echo File \'$file\' is either missing, empty, not named correctly, or has the wrong file extension.
                exit 1
            fi
        done
    else
        echo Directory 'webpages' missing
        exit 1
    fi

    echo "Congratulations $USER, you have completed $LAB4_VAR!\n \
            \rPlease follow the Submission Instructions in the Lab 4 document."

    if [ $VER_VAR = $(./uuidgen.sh) ]; then
        echo "UUID: $VER_VAR\
            \nDate: `date`\
            \nUsername: $USER"> lab4complete
    else
        echo "UUID mismatch!\
            \rVER_VAR: $VER_VAR\rUUID: $(./uuidgen.sh)" > lab4complete
    fi
else
    echo "File 'run.sh' either doesn't exist, or is not executable."
    exit 1
fi
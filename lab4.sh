#!/bin/sh

whoami
pwd
date

mkdir "$1"
chmod +x "$2"

if [ -x "$2" ]; then
    echo "$2 is executable file."
    while read -r line
    do
        echo curl "$line"
    web_name=$(./"$2" "$line")
    file="$1/$web_name.html"
    curl "$line" > "$file"
    done
fi

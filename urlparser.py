#!/bin/python3

import sys

def run():
    if len(sys.argv) != 2:
        print("Incorrect number of arguments.")
        exit(1)

    url = sys.argv[1]
    names = url.strip().split(".")

    print(names[1])
    return 0

run()
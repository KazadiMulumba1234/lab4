#!/bin/sh

if [ -e /etc/machine-id ]; then
    cat /etc/machine-id;
else
    echo $USER | tr aeiou oiuea;
fi